PREFIX = /usr

PACKAGE := $(shell dpkg-parsechangelog -l "debian/changelog" -S Source)
VERSION := $(shell dpkg-parsechangelog -l "debian/changelog" -S Version | cut -f 1 -d "+")

all: build

build:
	cargo install --root ${PACKAGE} --version ${VERSION} ${PACKAGE}

install:
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp ${PACKAGE}/bin/${PACKAGE} ${DESTDIR}${PREFIX}/bin/
	chmod 755 ${DESTDIR}${PREFIX}/bin/${PACKAGE}

clean:
	rm -rf ${PACKAGE}
