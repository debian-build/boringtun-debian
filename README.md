# Debian/Ubuntu packages for boringrun

## Installation

Just grab package for your distribution from CI and ```dpkg -i <package>.deb``` it.

## Usage
You can use this package in conjunction with [https://packages.debian.org/sid/wireguard-tools](wireguard-tools) package. Proper systemd unit override is included for wg-quick@.service.
